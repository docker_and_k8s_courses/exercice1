Dockerize BOTH apps - the Python and the Node app.

1) Create appropriate images for both apps (two separate images!).

docker build -f node-app/Dockerfile .

docker build -f python-app/Dockerfile .


2) Launch a container for each created image, making sure, 
that the app inside the container works correctly and is usable.

docker run --rm --name my_node_app_container -p 3000:8000 node_app
docker run --rm --name my_python_app_container -p 3000:8000 python_app


3) Re-create both containers and assign names to both containers.
Use these names to stop and restart both containers.

docker stop my_node_app_container
docker stop my_python_app_container



4) Clean up (remove) all stopped (and running) containers, 
clean up all created images.

docker rmi node_app python_app
docker rm my_node_app_container my_python_app_container



5) Re-build the images - this time with names and tags assigned to them.

docker build -t tp_node_app:latest -f node-app/Dockerfile .
docker build -t tp_python_app:latest -f python-app/Dockerfile .


6) Run new containers based on the re-built images, ensuring that the containers
are removed automatically when stopped.

docker run --rm --name my_node_app_container -p 3000:8000 node_app
docker run --rm --name my_python_app_container -p 3000:8000 python_app